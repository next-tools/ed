;;----------------------------------------------------------------------------------------------------------------------
;; Editor system
;;----------------------------------------------------------------------------------------------------------------------

EdIntro:
                db      2,C_ENTER,"Ed (Version Beta.5)",C_ENTER,C_ENTER,0
                db      7,"Copyright ",127,"2020 Matt Davies, all rights reserved",C_ENTER,0
                db      7,C_ENTER,0
                db      4,"Press H for help with commands and Ext+Q to quit.",C_ENTER,C_ENTER,0
                db      0

;;----------------------------------------------------------------------------------------------------------------------
;; Editor entry point
;;----------------------------------------------------------------------------------------------------------------------

editorMain:
                ;;
                ;; Set up
                ;;

                in      a,($e3)
                ld      (DivMMCPage),a
                call    initConsole

                call    initDoc                 ; Initialise the main document
                jp      c,.oom
                call    pageDoc                 ; And page it in

                call    initKeys

                ; Output Odin Logo
                call    pageVideo
                ld      hl,EdIntro
.l1:
                ldi     a,(hl)
                and     a
                jr      z,.done
                call    setColour
                call    printHL
                jr      .l1
.done:
                ld      a,kInkNormal
                call    setColour
                call    cursorOn

                ;;
                ;; Attempt to load a file if there is a filename
                ;;

                call    pageDivMMC
                ld      a,(FileName)
                and     a
                jr      z,.no_auto

                call    loadFile
                jr      nc,.no_auto

                ; The file wasn't found, so let's remind the user it's a new file
                call    pageVideo
                ld      a,kInkListing
                call    colouredPrint
                dz      "NEW FILE: "
                ld      hl,FileName
                call    printHL
                ld      a,kInkNormal
                call    colouredPrint
                dz      VK_ENTER, VK_ENTER

                ;;
                ;; Main loop
                ;;
.no_auto:
                xor     a
                ld      (AutoInsertMode),a
.main_loop:
                call    pageVideo               ; Page in the video
.waitKey
                call    consoleUpdate
                call    inKey
                jr      z,.waitKey

                ; Process key press
                cp      VK_EXTQ
                jp      z,.exit

                call    cursorHide
                call    printChar
                call    cursorShow

                cp      C_ENTER                 ; Did we press Enter?
                jr      nz,.waitKey             ; No, keep the input coming!

                ; Get input from line
                ld      hl,(CurrentPos)
                ld      de,160
                sbc     hl,de                   ; HL points to line that contains input
                ld      de,InputBuffer
                ld      b,81

                ; Skip past any whitespace?
.skip_ws:
                dec     b
                ld      a,b
                and     a
                jr      z,.main_loop

                ld      a,(hl)
                inc     hl
                inc     hl
                and     a
                jr      z,.skip_ws
                cp      $20
                jr      z,.skip_ws
                dec     hl
                dec     hl

.l2:
                ldi     a,(hl)
                and     a
                jr      nz,.copy
                ld      a,' '
.copy:          ldi     (de),a
                inc     hl
                djnz    .l2

                ; Convert all trailing spaces to nulls
.l3:
                dec     de
                ld      a,(de)
                cp      ' '
                jr      nz,.done_input
                xor     a
                ld      (de),a
                jr      .l3
.done_input:
                ld      de,InputBuffer

                ; Is it a line number or null terminator
                ld      a,(de)
                and     a
                jr      z,.no_auto
                cp      '0'
                jr      c,.not_number
                cp      '9'+1
                jr      nc,.not_number

                ; Transfer leading line number (and optional space) to buffer
                ld      hl,Buffer
.num_loop
                ldi     a,(de)
                cp      '0'
                jr      c,.end_num
                cp      '9'+1
                jr      nc,.end_num
                ldi     (hl),a
                jr      .num_loop
.end_num:       cp      ' '
                jr      z,.space
                dec     de
.space:

                call    tokeniseLine
                ld      de,Buffer
                call    docInsertLine

                ld      a,(AutoInsertMode)
                and     a
                jp      z,.main_loop

                ; Update current line number
                call    updateLineNumber

                jp      .main_loop

.not_number:
                xor     a
                ld      (AutoInsertMode),a

                ; Is it a possible command?
                ex      de,hl
                call    strSkipWS               ; Skip past any leading whitespace
                call    isAlpha
                jr      c,.error

                ; A = command letter
                ; Figure out if it's a command and the call the handler
                call    cmdHandle
                ld      a,kInkNormal
                call    setColour
                jp      .main_loop

.error:
                call    errorPrint
                dz      "ERROR"
                jp      .main_loop


                ;;
                ;; Exit
                ;;

.exit:
                call    doneDoc
                call    doneKeys
                call    doneConsole
                call    pageDivMMC
.oom:
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; typeLineNumber
;; Type the current auto-insert number to the screen
;;

updateLineNumber:
                ld      a,(AutoInsertMode)
                and     a
                ret     z                       ; Return if we're not in auto-insert mode

                call    pageVideo
                call    cursorHide
                ld      a,kInkListing
                call    setColour
                ld      a,C_CLEARLINE
                call    printChar

                ; Update line number by adding the increment
                ld      hl,(LineNumber)
                ld      a,(LineNumber+1)
                ld      e,a
                ld      bc,(InsertInc)
                add     hl,bc
                ld      a,(InsertInc+2)
                adc     a,e                     ; AHL = new line #

printLineNumber:
                call    cursorHide

                ; AHL = line #
                ; Type the line number and a space
                ld      bc,(CurrentCoords)
                ld      e,a
                call    unpackD24               ; DEHL = BCD version of number
                call    printBCD7
                ld      a,kInkNormal
                call    setColour
                ld      a,' '
                call    printChar

                ; Test to see if previous line started with a number
                ld      bc,(CurrentCoords)
                dec     b
                ld      c,0
                call    calcAddr
                ld      a,(hl)
                call    isDigit
                jr      c,.done

                ; Indent
                ; Calculate the address directly above the cursor and the end of the previous line
                ; We will insert spaces while there is whitespace above the character.
                ; However, if there is no text in the previous line, we do not indent.
                call    scrAddr
                ld      de,160
                and     a
                sbc     hl,de                   ; HL = tile address of previous line
                ex      de,hl                   ; DE = tile address of previous line
                ld      c,0                     ; Get end of previous line address
                call    calcAddr                ; HL = end of previous line
                ex      de,hl

                ; Count how many spaces in previous line
                ld      b,0
.l1:
                call    compare16               ; Reached end of line
                jr      z,.done                 ; No text on previous line so no indent
                ld      a,(hl)
                inc     hl
                inc     hl
                cp      $21
                jr      nc,.not_ws
                inc     b
                jr      .l1

.not_ws:
                ld      a,b
                and     a
                jr      z,.done

                ; B = number of spaces before text on previous line
.l2:
                ld      a,' '
                call    printChar
                djnz    .l2

.done:
                call    cursorShow

                ret


AutoInsertMode  db      0
InsertInc       d24     0

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
