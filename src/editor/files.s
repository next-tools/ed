;;----------------------------------------------------------------------------------------------------------------------
;; Deals with loading and saving files
;;----------------------------------------------------------------------------------------------------------------------

FileHandle:     db      0       ; Handle of currently opened file
FileLine:       d24     0       ; Current line number
FileStartLine:  dw      0       ; Start address of current line being read
FileStat:       ds      11

;;----------------------------------------------------------------------------------------------------------------------
;; fileOpen
;; Open a file in FileName, and try to read up to 8K.  It will also page in DivMMC.
;;
;; Output:
;;      CF = 1 if file not found or file too big

fileOpen:
                call    pageDivMMC

                ; Attempt to open the file
                xor     a
                dos     M_GETSETDRV
                ret     c
                ld      hl,FileName
                ld      b,FA_READ | FA_OPEN_EXISTING
                dos     F_OPEN
                ret     c
                ld      (FileHandle),a

                ; Make sure that the size is <= 48K
                ld      ix,FileStat
                dos     F_FSTAT
                ld      de,(ix+9)
                ld      a,d
                or      e
                scf
                ret     nz
                ld      hl,(ix+7)
                ld      de,$c001
                call    compare16
                ccf
                ret

fileOpenSave:
                xor     a
                dos     M_GETSETDRV
                ret     c
                ld      hl,FileName
                ld      b,FA_WRITE | FA_CREATE_NEW
                dos     F_OPEN
                ret     c
                ld      (FileHandle),a
                ld      hl,FileBuffer
                ret


fileInit:
                ; Attempt to read the first 256 bytes of data
                ld      a,(FileHandle)
                ld      hl,FileBuffer
                call    fileRead256
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fileDone
;; Finish reading a file
;;

fileDoneSaving:
                ; Flush the data in the current half-buffer.
                ld      a,l
                and     $80             ; A = beginning of half-buffer
                ld      d,a             ; Store offset
                sub     l
                neg                     ; A = offset - beginning = length
                ld      c,a
                ld      b,0
                ld      l,d             ; HL = beginning of buffer
                ld      a,(FileHandle)
                dos     F_WRITE
                ; continue into fileDone

fileDone:
                ld      a,(FileHandle)
                dos     F_CLOSE
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fileReadByte
;; Read a single byte from the buffer if you've used fileInit to initialise the read buffer.
;;
;; Input:
;;      HL = current buffer position
;;
;; Ouptut:
;;      A = byte
;;
;; Affects:
;;      BC
;;
;;

fileReadByte:
                ld      a,(hl)
                inc     l
                ret     nz

                ; Continue into fileRead128K

;;----------------------------------------------------------------------------------------------------------------------
;; fileRead256
;; Attempt to read in 128 bytes.  This routine will advance the current buffer pointer in HL
;;
;; Input:
;;      H = page of file buffer (FileBuffer/256)
;;      L = position in previous buffer
;;
;; Affects:
;;      AF, BC
;;

fileRead256:
                exa
                push    hl
                push    de
                ld      hl,FileBuffer
                ld      bc,256
                ld      a,(FileHandle)
                dos     F_READ
                pop     de
                jr      c,.full_read
                bit     0,b             ; Did we read the full 256 bytes?
                jr      nz,.full_read
                xor     a
                ld      (hl),a          ; Terminate the buffer
.full_read:     exa
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fileWriteByte
;; Write a single byte to the buffer if you've used fileInit to initialise the read buffer.
;;
;; Input:
;;      HL = current buffer position
;;
;; Ouptut:
;;      CF = 1 if error occurred
;;
;; Affects:
;;      BC
;;
;;

fileWriteByte:
                ld      (hl),a
                inc     l
                jp      pe,fileWrite128         ; $xx7f -> $xx80, load 128 bytes in first half of buffer
                ret     nz                      ; $xxff -> $0x00, load 128 bytes in second half of buffer

                ; Continue into fileWrite128K

;;----------------------------------------------------------------------------------------------------------------------
;; fileWrite128
;; Attempt to write out 128 bytes.  This routine will advance the current buffer pointer in HL
;;
;; Input:
;;      H = page of file buffer (FileBuffer/256)
;;      L = position in previous buffer
;;
;; Affects:
;;      AF, BC
;;

fileWrite128:
                exa
                push    hl
                ld      bc,$80
                ld      a,l
                add     a,c
                ld      l,a             ; Advance to buffer we just wrote to, wrapping around
                push    de
                ld      a,(FileHandle)
                dos     F_WRITE         ; Write the 128 bytes
                pop     de
                pop     hl
                exa
                ret

