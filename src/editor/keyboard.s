;;----------------------------------------------------------------------------------------------------------------------
;; Keyboard routines
;; Lifted and adapated from:
;;
;; https://github.com/z88dk/z88dk/blob/master/libsrc/_DEVELOPMENT/input/zx/z80/asm_in_inkey.asm
;; https://github.com/z88dk/z88dk/blob/master/libsrc/_DEVELOPMENT/input/zx/z80/in_key_translation_table.asm
;;
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Keyboard constants

BUFFERPAGE      equ     $3e     ; $3e00 holds 256 bytes for keyboard buffer
KEYB_ORG        equ     $2000   ; $2000 start of keyboard code (jumps immediately to Start)
KEYB_IPAGE      equ     $e1     ; Interrupt page for 257 interrupt table
KEYB_ADDR       equ     $e0e0   ; Address of interrupt routine (MSB must equal LSB)

KEYREP_DELAY    equ     40      ; Number of frames until we start repeating key
KEYREP_REP      equ     2       ; Must be 2^n value

;;----------------------------------------------------------------------------------------------------------------------
;; IM setup and shut-down


OldI            db      0

initKeys:
                di
                ld      a,i
                ld      (OldI),a
                ld      a,KEYB_IPAGE
                ld      i,a
                im      2
                ei
                ret

doneKeys:       di
                ld      a,(OldI)
                ld      i,a
                im      1
                ret

                org     KEYB_ADDR

                jp      ImRoutine

                org     (KEYB_IPAGE << 8)
                ds      257,(KEYB_ADDR & $ff)

; Scans the keyboard and returns button code - runs in IM 2
;
; Rows are:
;   Bits:   0       1       2       3       4
;   ----------------------------------------------
;   $FE     Caps    Z       X       C       V
;   $FD     A       S       D       F       G
;   $FB     Q       W       E       R       T
;   $F7     1       2       3       4       5
;   $EF     0       9       8       7       6
;   $DF     P       O       I       U       Y
;   $BF     Enter   L       K       J       H
;   $7F     Space   Sym     M       N       B
;
; Keyboard ASCII codes (00-1F)
;
;   00                      10  Sym+W
;   01  Edit                11  Sym+E
;   02  Capslock            12  Sym+I
;   03  True Video          13  
;   04  Inv Video           14  
;   05  Left                15  
;   06  Down                16  
;   07  Up                  17  
;   08  Right               18  
;   09  Graph/TAB           19  
;   0A  Delete              1A  
;   0B                      1B  Break (Caps & Space)
;   0C                      1C  (Sym & Space)
;   0D  Enter               1D  Shift+Enter
;   0E                      1E  Sym+Enter
;   0F                      1F           
;
; Keyboard ASCII codes (80-FF)
;
;   80 - Extended Mode
;   8D - Ext+Enter
;   A0 - Ext+Space
;   B0-B9 - Ext+Number
;   E1-FA - Ext+Letter

;;----------------------------------------------------------------------------------------------------------------------
;; Keyboard interrupt routine

ImRoutine:
                di
                push    af
                push    bc
                push    de
                push    hl
                push    ix
                xor     a
                ld      (KeysPressed),a
                call    KeyScan         ; HL = Keyboard table

                ; Update shift statuses
                push    hl
                pop     ix
                xor     a
                bit     0,(ix+14)       ; Test for Caps (row 7 * 2 bytes)
                jr      z,.no_caps
                or      40
.no_caps        bit     1,(ix+12)       ; Test for symbol shift (row 6 * 2 bytes)
                jr      z,.no_sym
                or      80
.no_sym         ex      de,hl           ; DE = keyboard snapshot
                ld      hl,KeyTrans
                add     hl,a            ; HL = Key translation table

                ld      ix,KBufferState ; Required for buffer routines
                ; Now we scan our keyboard snapshot, and any keys that are detected to be pressed are added to
                ; the circular buffer
                ld      b,8
.row            
                ; Grab the next keyscan of a group of keys and mask out any shift keys
                ld      a,b
                push    hl
                ld      hl,ValidKeyMask-1
                add     hl,a
                ld      a,(de)          ; A = keyboard state for current row
                and     (hl)
                pop     hl

                ; If a non-shift key is pressed, then write a non-zero value to KeysPressed
                ; so that we indicate a key is still pressed.  This is required for the key repeat logic.
                and     a
                jr      z,.no_key
                ld      (KeysPressed),a

.no_key
                ; Compare the current key configuration with the last one to see which keys are freshly
                ; pressed.
                inc     de
                ld      c,a             ; C = keyboard state for current row
                ld      a,(de)          ; A = last keyboard state
                inc     de
                xor     $ff
                and     c               ; A = edge detected key state (!A + C)
                push    hl              ; Store the current key translation table position

.col            and     a               ; Any keys pressed on entire row?
                jr      z,.end_row

                ; Find the first key that is pressed and convert it to an ASCII character using
                ; the key table.
                srl     a               ; Key pressed?
                jr      nc,.ignore

                ld      c,(hl)          ; C = ASCII character

                ; Insert key (C) into circular buffer
                push    bc
                ld      b,BUFFERPAGE
                call    BufferInsert    ; Insert into circular buffer
                pop     bc

                ; Update last key if necessary
                ld      a,c
                ld      (LastKey),a     ; Reset the last key
                xor     a
                ld      (KeyCounter),a  ; And counter

.ignore         inc     hl              ; Next entry into table
                jr      .col

.end_row        ld      a,5
                pop     hl
                add     hl,a            ; Next row of table
                djnz    .row

                ; Any keys pressed?  If not, reset key counter
                ld      a,(KeysPressed)
                and     a
                jr      nz,.fetch
                ld      (LastKey),a
                ld      (KeyCounter),a

                ; Fetch a character
.fetch
                ld      hl,KFlags
                bit     0,(hl)
                jr      nz,.finish      ; Still haven't processed last key yet

                ld      b,BUFFERPAGE
                call    BufferRead
                jr      z,.no_chars
                ld      (Key),a         ; Next key available
                set     0,(hl)          ; Key ready!
                jr      .finish

.no_chars       xor     a
                ld      (Key),a

.finish
                ; Advance counter
                ld      hl,(Counter)
                inc     hl
                ld      (Counter),hl

                ; Deal with key repeat
                ld      a,(LastKey)
                and     a
                jr      z,.no_repeat
                ld      a,(KeyCounter)
                inc     a
                ld      (KeyCounter),a
                sub     KEYREP_DELAY    ; Equal to initial key repeat
                jr      c,.no_repeat
                jr      z,.start_repeat
                and     KEYREP_REP      ; Trigger 2 or more repeats
                jr      z,.no_repeat
                ld      a,KEYREP_DELAY
                ld      (KeyCounter),a

.start_repeat   
                ld      b,BUFFERPAGE
                ld      a,(LastKey)
                ld      c,a
                call    BufferInsert

.no_repeat
                pop     ix
                pop     hl
                pop     de
                pop     bc
                pop     af
                ei
                reti

KBufferState    dw      BUFFER_START
KeysPressed     db      0
Keys:           ds      16      ; Double-buffered interleaved space to store the key presses
                                ; NEW OLD NEW OLD NEW OLD...
                                ; Thought about having a dynamic pointer to switch buffers but it turns out
                                ; that having fixed buffers makes things easier later on
ValidKeyMask    db      $fe, $fd, $ff, $ff, $ff, $ff, $ff, $ff

KeyScan:
                ; Output:
                ;   HL = pointer to keyscan
                ; Destroys
                ;   BC, DE, A

                ld      hl,Keys

                ; Scan the keyboard
                ld      bc,$fdfe        ; Keyboard ports (start here to make sure shift rows are last)
                push    hl
                ld      e,8             ; There are 8 ports to read

.l1             ld      d,(hl)          ; Get old state
                in      a,(c)
                cpl
                and     $1f
                ld      (hl),a          ; Store new state
                inc     hl
                ld      (hl),d          ; Store old state
                inc     hl
                rlc     b
                dec     e
                jr      nz,.l1
                pop     hl          ; Restore pointer to buffer
                ret

Test    db 0

;;----------------------------------------------------------------------------------------------------------------------

Key:            db      0               ; Latest ASCII character
KFlags:         db      0               ; Bit 0 = character available, reset when test
Counter:        dw      0               ; 50/60Hz counter

LastKey:        db      0               ; Last key inserted into key buffer
KeyCounter:     db      0               ; Current counter since last key changed (used for repeat)

;;----------------------------------------------------------------------------------------------------------------------

KeyTrans:
        ; Unshifted
        db      'a','s','d','f','g'                             ; A-G
        db      'q','w','e','r','t'                             ; Q-T
        db      '1','2','3','4','5'                             ; 1-5
        db      '0','9','8','7','6'                             ; 0-6
        db      'p','o','i','u','y'                             ; P-Y
        db      $0d,'l','k','j','h'                             ; Enter-H
        db      ' ',$ff,'m','n','b'                             ; Space-B
        db      $ff,'z','x','c','v'                             ; Caps-V

        ; CAPS shifted
        db      'A','S','D','F','G'                             ; A-G
        db      'Q','W','E','R','T'                             ; Q-T
        db      $01,$02,$03,$04,$05                             ; 1-5
        db      $0a,$09,$08,$07,$06                             ; 0-6
        db      'P','O','I','U','Y'                             ; P-Y
        db      $1d,'L','K','J','H'                             ; Enter-H
        db      $1b,$ff,'M','N','B'                             ; Space-B
        db      $ff,'Z','X','C','V'                             ; Caps-V

        ; SYM shifted
        db      '~','|','\','{','}'                             ; A-G
        db      $7f,'w','e','<','>'                             ; Q-T
        db      '!','@','#','$','%'                             ; 1-5
        db      '_',')','(',$27,'&'                             ; 0-6
        db      $22,';','i',']','['                             ; P-Y
        db      $1e,'=','+','-','^'                             ; Enter-H
        db      $1c,$ff,'.',',','*'                             ; Space-B
        db      $ff,':','`','?','/'                             ; Caps-V

        ; EXT shifted
        db      'a'+$80,'s'+$80,'d'+$80,'f'+$80,'g'+$80         ; A-G
        db      'q'+$80,'w'+$80,'e'+$80,'r'+$80,'t'+$80         ; Q-T
        db      '1'+$80,'2'+$80,'3'+$80,'4'+$80,'5'+$80         ; 1-5
        db      '0'+$80,'9'+$80,'8'+$80,'7'+$80,'6'+$80         ; 0-6
        db      'p'+$80,'o'+$80,'i'+$80,'u'+$80,'y'+$80         ; P-Y
        db      $8d,    'l'+$80,'k'+$80,'j'+$80,'h'+$80         ; Enter-H
        db      $a0,    $ff,    'm'+$80,'n'+$80,'b'+$80         ; Space-B
        db      $ff,    'z'+$80,'x'+$80,'c'+$80,'v'+$80         ; Caps-V

;;----------------------------------------------------------------------------------------------------------------------
;; WaitKey
;;       Wait until a key is pressed and return virtual key value
;;
;; Input:
;;       None
;; Output:
;;       A = virtual key
;;

waitKey:
                call    inKey
                jr      z,waitKey
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; inKey
;;      Try to get a key press if there is one.
;; Input:
;;      None
;; Output:
;;      A = virtual key (if ZF == 0)
;;      ZF = 1 if there is no key (and A = 0)

inKey:
                xor     a
                push    hl
                ld      hl,KFlags
                bit     0,(hl)
                jr      z,.done

                ld      a,(Key)
                res     0,(hl)

.done           pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; flushKeys
;; Clear key buffer
;;

flushKeys:
                call    inKey
                ret     z
                halt
                jr      flushKeys

;;----------------------------------------------------------------------------------------------------------------------
;; Circular buffer

BUFFER_START    equ     $0100

; Empty buffer:
;
;       +-------------------------------------------+
;                       ^^
;                       RW
;
;       R points to just before read point
;       W points at new place to write
;       R should never meet W while reading
;
; Full buffer
;       +XXXXXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXX+
;                       ^
;                       R
;                       W

BufferInsert:
                ; Input:
                ;       B = Buffer page
                ;       C = Value to insert
                ;       IX = Pointer to read/write pointers
                ; Note:
                ;       The value that IX points to must be a 16-bit value initialised to BUFFER_START
                ;
                push    hl
                push    af

                ld      a,(ix+0)
                cp      (ix+1)          ; Has PWrite reached PRead yet?
                ret     z               ; Return without error (buffer is full)

                ld      l,(ix+1)
                ld      h,b             ; HL = write address
                ld      (hl),c          ; write value in buffer
                inc     (ix+1)
                pop     af
                pop     hl
                ret

BufferRead:     ; Input:
                ;       B = Buffer page
                ;       IX = Pointer to read/write pointers
                ; Output
                ;       A = Value
                ;       B = Buffer page
                ;       ZF = 1 if nothing to read
                ; Note:
                ;       The value that IX points to must be a 16-bit value initialised to BUFFER_START
                ;
                push    hl
                ld      a,(ix+1)
                dec     a
                cp      (ix+0)            ; Buffer is empty?
                jr      z,.finish

                inc     (ix+0)            ; Advance read pointer
                ld      l,(ix+0)
                ld      h,b             ; HL = buffer pointer
                ld      a,(hl)          ; Read data
                and     a               ; Clear ZF
.finish         pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Key codes

VK_EDIT         equ     $01
VK_CAPSLOCK     equ     $02
VK_TRUEVIDEO    equ     $03
VK_INVVIDEO     equ     $04
VK_LEFT         equ     $05
VK_DOWN         equ     $06
VK_UP           equ     $07
VK_RIGHT        equ     $08
VK_GRAPH        equ     $09
VK_DELETE       equ     $0a
VK_ENTER        equ     $0d
VK_SYMW         equ     $10
VK_SYME         equ     $11
VK_SYMI         equ     $12
VK_BREAK        equ     $1b
VK_SYMSPACE     equ     $1c
VK_SHIFTENTER   equ     $1d
VK_SYMENTER     equ     $1e

VK_EXTENTER     equ     $8d
VK_EXTSPACE     equ     $a0
VK_EXT0         equ     $b0
VK_EXT1         equ     $b1
VK_EXT2         equ     $b2
VK_EXT3         equ     $b3
VK_EXT4         equ     $b4
VK_EXT5         equ     $b5
VK_EXT6         equ     $b6
VK_EXT7         equ     $b7
VK_EXT8         equ     $b8
VK_EXT9         equ     $b9

VK_EXTA         equ     $e1
VK_EXTB         equ     $e2
VK_EXTC         equ     $e3
VK_EXTD         equ     $e4
VK_EXTE         equ     $e5
VK_EXTF         equ     $e6
VK_EXTG         equ     $e7
VK_EXTH         equ     $e8
VK_EXTI         equ     $e9
VK_EXTJ         equ     $ea
VK_EXTK         equ     $eb
VK_EXTL         equ     $ec
VK_EXTM         equ     $ed
VK_EXTN         equ     $ee
VK_EXTO         equ     $ef
VK_EXTP         equ     $f0
VK_EXTQ         equ     $f1
VK_EXTR         equ     $f2
VK_EXTS         equ     $f3
VK_EXTT         equ     $f4
VK_EXTU         equ     $f5
VK_EXTV         equ     $f6
VK_EXTW         equ     $f7
VK_EXTX         equ     $f8
VK_EXTY         equ     $f9
VK_EXTZ         equ     $fa

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

