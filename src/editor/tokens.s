;;----------------------------------------------------------------------------------------------------------------------
;; Lexical analysis and tokens
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; tokenise
;; Tokenise the next token in the input according to the given table
;;
;; Input:
;;      DE = input
;;      HL = token table (in ASCII order)
;;      B = Index start (usually $80)
;;
;; Output:
;;      CF = 1 if not found
;;      ZF = 1 if found
;;      HL = Input buffer
;;      DE = New position within input buffer after token
;;      B = index of first token
;;

tokenise:
                inc     b
                ld      a,(de)          ; Get first character
                call    upperCase
                cp      (hl)            ; Compare with first character in table
                ret     c               ; Input character < table entry?  We didn't find it's
                jr      z,.same         ; We found a matching character

                ; Input and command entry different.  Find the end of the entry and repeat on next one.
.l1:            ldi     a,(hl)          ; Load next character
                cp      ' '
                jr      nc,.l1          ; Keep going until we find a terminator
                cp      $08
                jr      z,tokenise
                cp      $0a
                jr      z,tokenise

                ; No more entries in table
                scf
                ret

.same:
                push    de              ; Store start of input buffer
.l2:
                inc     de              ; On to next input character
                inc     hl              ; Next character in token
                ld      a,(de)
                call    upperCase
                cp      (hl)
                jr      z,.l2           ; They're still matching - keep going

                ld      a,(hl)          ; Get the unmatching character or a terminator
                cp      $08             ; Did we match the whole token?
                jr      z,.accept       ; Yes!
                cp      $0a
                jr      nz,.next        ; No?  Not a match - find the next one

                ; We matched the whole token, but it might be part of a longer token with the same prefix.  If there is
                ; an identifier character following it, it's not a match and subsequent tokens in the table may still
                ; match.
                ld      a,(de)
                call    isIdentChar
                jr      c,.accept       ; This was not an identifier character, so we can accept it.

.next:
                pop     de              ; Back to where we were on the input buffer to try next match
                jr      .l1             ; Next token to test
.accept:
                pop     hl              ; HL = input buffer
                or      a               ; ZF = 1 if found
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; tokeniseLine
;; Compress spaces in the input buffer
;;
;; Input:
;;      HL = Output buffer
;;      DE = Input buffer
;;
;; Output:
;;      C = number of spaces at end of line
;;

tokeniseLine:
                ld      c,0
                jr      .start

.accept:
                inc     hl
.no_accept:
                inc     de

.start:
                ld      a,(de)
                ld      (hl),a          ; Copy character
                or      a               ; Reached end?
                ret     z

                cp      ' '             ; Was character a space?
                jr      nz,.not_space
                inc     c               ; Count the spaces
                jr      .no_accept      ; But don't accept it yet

.not_space
                ; C = number of spaces seen before now
                dec     c               ; ZF will set if we only have one space
                jr      z,.single_space
                inc     c               ; ZF will be set if we had no spaces beforehand
                jr      z,.no_spaces

                ; Compress spaces
                ldi     (hl),$0a        ; $0A is a token for space compression
                ldi     (hl),c          ; Insert space count
                ld      c,0
                jr      .no_spaces
.single_space:
                ldi     (hl),' '
.no_spaces:
                ld      a,(de)
                ld      (hl),a          ; Store first character
                jr      .accept

;;----------------------------------------------------------------------------------------------------------------------
;; detokenise
;; Detokenise a line into a buffer.
;;
;; Input:
;;      HL = input buffer
;;      DE = output buffer (256-byte aligned)
;;
;; Output:
;;      CF = 1 if not found
;;      ZF = 1 if found
;;      HL = Input buffer
;;      DE = New position within input buffer after token
;;

detokenise:
                jr      .start
.accept:
                inc     e
                jr      z,.overflow

.no_accept:
                inc     hl
.start:
                ld      a,(hl)
                cp      $0a             ; Compressed space?
                jr      z,.compressed

.l1:
                ld      (de),a
                or      a
                jr      nz,.accept

                ret                     ; Reached end of buffer

.compressed:
                inc     hl
                ld      b,(hl)          ; Get count of spaces
                ld      a,' '
.l3:
                ld      (de),a
                inc     e
                jr      z,.overflow
                djnz    .l3
                jr      .no_accept

.overflow:
                dec     e
                xor     a
                ld      (de),a
                ret
