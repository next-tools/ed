;;----------------------------------------------------------------------------------------------------------------------
;; Command handlers
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Command tables

CmdNameTable:
                dz      "H"             ; Help
                dz      "P"             ; Print
                dz      "S"             ; Save
                dz      "L"             ; Load
                dz      "I"             ; Insert
                dz      "R"             ; Renumber
                dz      "D"             ; Delete
                db      0

CmdTable:
                ; Help
                dw      cmdHelp
                db      0

                ; Print <start> <end>
                dw      cmdPrint
                db      2               ; Number of parameters
                db      'n'
                dd      0               ; Default: Line 0
                db      'n'
                dd      kMaxLine        ; Default: last line

                dw      cmdTextSave
                db      1
                db      's'
                dd      0

                dw      cmdTextLoad
                db      1
                db      's'
                dd      0

                ; Insert <start> <inc>
                dw      cmdInsert
                db      2
                db      'n'
                dd      10
                db      'n'
                dd      10

                ; Renumber <first> <inc> <line>
                dw      cmdRenumber
                db      3
                db      'n'
                dd      10
                db      'n'
                dd      10
                db      'n'
                dd      0

                ; Delete <from> <to>
                dw      cmdDelete
                db      2
                db      'n'
                dd      0
                db      'n'
                dd      0

;;----------------------------------------------------------------------------------------------------------------------
;; cmdHandle
;; Fetch the parameters in the input, validate them and call the handler
;;
;; Input:
;;      HL = Input buffer
;;      A = command letter

cmdHandle:
                ; Find the command in the table
                ex      de,hl
                ld      hl,CmdNameTable
                ld      b,0
                push    de
.l1:
                ; Reached end of current command?
                ld      a,(hl)
                and     a
                jr      z,.end_cmd              ; End of command found, check input buffer to see if it ended.

                ld      a,(de)                  ; Get command
                call    upperCase
                cp      (hl)
                jr      nz,.next_cmd

                inc     hl
                inc     de
                jr      .l1
.next_cmd:
                ldi     a,(hl)
                and     a
                jr      nz,.next_cmd
                inc     b
                pop     de
                push    de
                ld      a,(hl)
                and     a                       ; Found end of command list?
                jr      z,.unknown
                jr      .l1
.end_cmd:
                ; Here the input has matched a command so far.  The input may still have extra characters.
                ld      a,(de)
                call    isAlpha
                jr      c,.found_cmd            ; Not a letter: so end of command in input buffer too
                jr      .next_cmd

.found_cmd:
                pop     hl      ; Discard the input buffer pointer

                ; B = index
                ; Find the command details
                ld      hl,CmdTable
                ld      a,b
                and     a
                jr      z,.found_entry
.l2:
                inc     hl
                inc     hl              ; Skip past the handler address
                ldi     a,(hl)          ; A = number of parameters
                ld      c,a
                add     a,a
                add     a,a
                add     a,c
                add     hl,a            ; Skip past the parameter defaults (4 bytes per parameter)
                djnz    .l2
.found_entry:

                ; Fetch the first 3 parameters if they exist
                ld      ix,CmdBuffer
                ex      de,hl
                call    strSkipWS       ; HL = start of first parameter

                ; DE = Command entry
                ; HL = Input buffer at first parameter
                ; IX = Command parameter buffer
                ;
                push    de
                inc     de
                inc     de
                ldi     a,(de)
                and     a
                ld      b,a             ; B = number of parameters
                jr      z,.run_cmd
.next_param
                push    bc
                call    cmdFetch
                pop     bc
                jr      c,.invalid
                djnz    .next_param
                jr      .run_cmd

.invalid:
                pop     de
                call    errorPrint
                dz      "INVALID PARAMETER"
                ret

.run_cmd:
                pop     de
                ld      ix,CmdBuffer
                xor     a
                ex      de,hl           ; HL = Command entry
                ldhl                    ; HL = Command handler, IX = Command parameter buffers
                jp      (hl)

.unknown:
                pop     de
                call    errorPrint
                dz      "UNKNOWN COMMAND"
                ret

                ;; Command buffer per entry:
                ;;      0 Union of:
                ;;              Line number: 1-3: Line # (little endian)
                ;;              String: 1-2: address of start, 3-4: address of end
                ;;
                ;; 3 commands max
CmdBuffer       ds      4*3

;;----------------------------------------------------------------------------------------------------------------------
;; cmdFetch
;; Fetch the next parameter.
;;
;; Input:
;;      DE = address of first parameter info
;;      HL = input buffer
;;      IX = current parameter buffer
;;
;; Output:
;;      DE = address of next parameter info
;;      HL = input buffer for next parameter or at null terminator
;;      IX = next parameter buffer
;;      CF = 1 if bad parameter
;;

cmdFetch:
                ; Attempt to categorise parameter
                ld      a,(hl)
                and     a
                jr      z,.default      ; If reached end of input?
                cp      '0'
                jr      c,.not_num
                cp      '9'+1
                jr      nc,.not_num

                ; It's a numerical parameter
                ld      a,(de)
                cp      'n'
                scf
                ret     nz              ; Not expecting a number so return error

                push    de
                call    strDec          ; DEHL = number, BC = buffer after number
                call    checkLineNumber
                ld      a,e
                pop     de
                ccf
                ret     c               ; Return if number is not valid
                
                ld      (ix+0),l
                ld      (ix+1),h
                ld      (ix+2),a        ; Store it
                ld      hl,bc
                jr      .next

.not_num:
                ld      a,(de)
                cp      's'
                jr      nz,.not_string
                ld      a,(hl)
                cp      $22             ; Is it a string
                jr      nz,.no_quote
                inc     hl              ; Skip past quote
                ld      (ix+0),hl       ; Store start address
.l1:
                ; Search for end of string
                ld      a,(hl)
                and     a
                jr      z,.end_str
                cp      $22
                jr      z,.end_str
                inc     hl
                jr      .l1
.end_str:
                ld      (ix+2),hl       ; Store end address
                inc     hl
                jr      .next

.no_quote:
                ld      (ix+0),hl
.l2:
                ld      a,(hl)
                and     a
                jr      z,.end_str
                cp      ' '+1
                jr      c,.end_str
                inc     hl
                jr      .l2

.not_string:
                scf
                ret

.default:
                inc     de
                ldi     a,(de)
                ld      (ix+0),a
                ldi     a,(de)
                ld      (ix+1),a
                ldi     a,(de)
                ld      (ix+2),a
                ldi     a,(de)
                ld      (ix+3),a
                jr      .next2

.next:
                ld      a,5
                add     de,a
.next2:
                inc     ix
                inc     ix
                inc     ix
                inc     ix
                call    strSkipWS
                and     a

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdHelp

cmdHelp:
                call    print
                db      C_CLEARLINE,"Commands (case insensitive):",C_ENTER
                db      C_CLEARLINE,"  P <start> <end>          List program",C_ENTER
                db      C_CLEARLINE,"  L <filename>             Load in a document",C_ENTER
                db      C_CLEARLINE,"  S <filename>             Save out a document",C_ENTER
                db      C_CLEARLINE,"  I <start> <inc>          Start auto-insert mode",C_ENTER
                db      C_CLEARLINE,"  R <start> <inc> <line>   Renumber lines",C_ENTER
                db      C_CLEARLINE,"  D <start> <end>          Delete lines",C_ENTER
                db      C_CLEARLINE,C_ENTER
                db      C_CLEARLINE,"Keys:",C_ENTER
                db      C_CLEARLINE,"  Ext+Q                    Quit",C_ENTER
                db      C_CLEARLINE,"  Ext+L                    Clear screen",C_ENTER
                db      C_CLEARLINE,"  Ext+O                    Toggle overwrite mode",C_ENTER
                db      C_CLEARLINE,"  Edit                     Clear to end of line",C_ENTER
                db      C_CLEARLINE,"  Ext+Edit                 Clear whole line",C_ENTER
                db      C_CLEARLINE,"  Ext+Cursor Left          Beginning of line",C_ENTER
                db      C_CLEARLINE,"  Ext+Cursor Right         End of line",C_ENTER
                db      C_CLEARLINE,"  True Video               Delete forward",C_ENTER
                db      C_CLEARLINE,"  Inv Video                Tab",C_ENTER
                db      C_CLEARLINE,C_ENTER
                db      0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docPrintLine
;; Print the line currently pointed to by HL.
;;
;; Document is paged in on exit.
;;
;; Input:
;;      HL = line
;;

docPrintLine:
                push    ix,de,hl
                call    pageVideo
                call    cursorHide
                call    pageDoc

                dec     hl
                dec     hl
                dec     hl              ; HL = point to line #

                ; Copy line to buffer
                ld      de,Buffer
                ldi
                ldi
                ldi                     ; Copy line to buffer
                call    detokenise
                ld      hl,Buffer

                call    pageVideo
                ld      a,C_CLEARLINE
                call    printChar       ; Clears the current line

                ldi     e,(hl)
                ldi     d,(hl)
                ex      de,hl
                ldi     a,(de)          ; DE = text
                push    de
                ld      e,a             ; EHL = value to print
                call    unpackD24       ; DEHL = BCD representation of the number

                ; Output the 7-digit number
                call    printBCD7

                ; Output a space
                ld      a,' '
                call    printChar

                ; Output text
                pop     hl
                ld      b,72
                call    printHLWidth
                ld      a,C_ENTER
                call    printChar

                call    cursorShow
                call    pageDoc
                pop     hl,de,ix
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdPrint
;; Output the lines of the text file
;;
;; Syntax:
;;      P <start> <end>
;;

cmdPrint:
                ld      a,kInkListing
                call    setColour

                ; Find the first line
                ld      hl,(ix+0)
                ld      c,(ix+2)
                push    ix
                call    docFindLine     ; CDE = line, HL = address
                pop     ix
                ret     c               ; Reached end of document

.next_line:
                ; Check to see if we're passed the required line
                ; IX+4-6 is the end line number, CDE is the line #
                ; If end line < current line then stop

                ; MSB
                ld      a,(ix+6)
                cp      c               ; END2 < L2?
                jr      c,.done
                jr      nz,.not_yet

                ; Middle byte
                ld      a,(ix+5)
                cp      d               ; END1 < L1?
                jr      c,.done
                jr      nz,.not_yet

                ; LSB
                ld      a,(ix+4)
                cp      e               ; END0 < L0?
                jr      c,.done

.not_yet:
                ; Ok, this line is ok to print.  Start with the line number
                call    docPrintLine 
                call    checkLineCount
                jr      z,.done

                ; Find the start of the next line
                call    pageDoc
                call    strEnd
                inc     hl
                ldi     de,(hl)
                ldi     c,(hl)
                jr      .next_line
.done:
                call    flushKeys
                ld      a,kInkNormal
                call    setColour
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; checkLineCount
;; Check to see if the screen as been filled yet
;;
;; Output:
;;      ZF = 1 if break is pressed
;;

checkLineCount:
                ld      a,(LineCount)
                inc     a
                ld      (LineCount),a
                cp      kScreenHeight-1
                jr      z,.paused
                call    breakPressed
                ret

.paused:
                call    pageVideo
                call    cursorHide
                ld      a,kInkNormal
                call    colouredPrint
                dz      C_CLEARLINE,"[Press BREAK to stop]"
                xor     a
                ld      (LineCount),a
                ld      a,kInkListing
                call    setColour
                call    cursorShow

                call    waitNoKey
                call    waitAnyKey
                call    print
                dz      C_CLEARLINE
                call    breakPressed
                ret

LineCount       db      0

;;----------------------------------------------------------------------------------------------------------------------
;; cmdTextSave
;;

cmdTextSave:
                call    saveFileFromCmd
                ret     nc
                call    pageVideo
                ld      a,kInkError
                call    colouredPrint
                dz      C_CLEARLINE,"UNABLE TO WRITE FILE: "
                call    cursorHide
                ld      hl,FileName
                call    printHL
                call    cursorShow
                call    print
                dz      C_ENTER
                ret

saveFileFromCmd:
                call    pageDivMMC  
                ld      a,(ix+0)
                or      (ix+1)
                jr      nz,.load

                ; No name given: use previous name
                ld      a,(FileName)
                and     a
                jp      z,.no_name
                jr      .start

.load:
                ; Set up the filename from the parameters
                ld      hl,(ix+0)
                ld      bc,(ix+2)
                ld      de,FileName
                call    strSliceCopy
            
.start:
                call    fileOpenSave            ; HL = buffer
                ret     c

                exx
                ld      bc,0                    ; Keep C = 0 for DeTok
                ld      hl,addrMMU4

.next_page:
                call    pageDocSection          ; $8000-$9fff has section of document

.next_line:
                inc     hl
                inc     hl
                ldi     a,(hl)
                cp      $ff             ; Reached end of document?
                jr      z,.done

                ; Detokenise line into buffer
                ld      de,Buffer
                push    de
                call    detokenise
                pop     de
                inc     hl
                push    bc

.next_char:
                ldi     a,(de)
                and     a
                jr      z,.eol
                exx
                call    fileWriteByte
                exx
                jr      .next_char
.eol:
                ld      a,$0a
                exx
                call    fileWriteByte
                exx
                pop     bc

                ; Test to see if we've crossed the page
                ld      a,h
                cp      $a0
                jr      c,.next_line
                inc     b
                sub     $20
                ld      h,a
                jr      .next_page

.done:
                exx
                call    fileDoneSaving

                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"Text file saved: "
                call    cursorHide
                ld      hl,FileName
                call    printHL
                call    cursorShow
                call    print
                dz      C_ENTER
                ret

.no_name:
                call    errorPrint
                dz      "No filename given"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdTextLoad

cmdTextLoad:
                call    loadFileFromCmd
                ret     nc
                call    errorPrint
                dz      "FILE NOT FOUND"
                ret

loadFileFromCmd:
                ld      a,(ix+0)
                or      (ix+1)
                jp      z,loadFile

                ; Set up the filename from the parameters
                ld      hl,(ix+0)
                ld      bc,(ix+2)
                ld      de,FileName
                call    strSliceCopy
            
loadFile:
                call    fileOpen
                ret     c
                call    fileInit        ; HL = buffer
                exx

                xor     a
                ld      h,a
                ld      l,a
                ld      (.line_number),a
                ld      (.line_number+1),hl     ; Line number = 0
                ld      hl,addrMMU4             ; Start of document (MMU4)
                ld      (.smc_page+1),a

.next_page:
.smc_page       ld      b,0
                call    pageDocSection          ; Ensure window on document is on MMU4-5
                ld      a,(.smc_page+1)
                inc     a
                ld      (.smc_page+1),a         ; Increment page window

.next_line:
                ; Read next input byte
                exx
                ld      a,(hl)
                and     a
                jp      z,.done                 ; Finished reading document
                exx

                ; Increment line number and write it
                push    hl
                ld      hl,(.line_number)
                ld      a,(.line_number+2)      ; AHL = line number
                ld      bc,10
                add     hl,bc
                adc     a,0                     ; AHL = AHL + 10
                ld      (.line_number),hl
                ld      (.line_number+2),a
                ex      de,hl                   ; ADE = line number
                pop     hl                      ; Restore buffer pointer
                ldi     (hl),e
                ldi     (hl),d
                ldi     (hl),a

                ; Check to see if the writing of the line number didn't overflow.  If it did,
                ; then we have to delete the last line read as that's the only place where there's
                ; space to write an end-of-document marker.
                ld      a,h
                cp      $a0
                jr      c,.no_boundary
                sub     $20
                ld      h,a

                ; Check to see we haven't broken the 48K boundary
                ld      a,(.smc_page+1)
                cp      6
                jr      z,.overflow             ; No we're OK!

.no_boundary:
                ; Read in next line into buffer
                exx
                ld      de,Buffer
.l1:
                call    fileReadByte            ; A = next byte
                and     a
                jr      z,.eof
                cp      $0d
                jr      z,.l1                   ; Ignore $0d, and only respond to $0a
                cp      $0a
                jr      z,.finish_read
                ld      (de),a
                inc     e
                jr      nz,.l1                  ; Keep reading bytes until we find the end of the line

                ; Run out of bytes in the buffer (line length > 255)
                dec     e

                ; Find the end of the current line
.l2:
                call    fileReadByte
                and     a
                jr      z,.done                 ; Reached end of file so no more lines to read
                cp      $0a
                jr      nz,.l2
                jr      .finish_read

.eof:
                dec     hl                      ; Force to read the null terminator again

.finish_read:
                ; Buffer contains null or $0a terminated line
                xor     a
                ld      (de),a                  ; Null terminate line
                exx
                ld      de,Buffer
                ld      (.last_line),hl         ; Save the start of the last line

                call    tokeniseLine            ; HL = end of line on null terminator
                inc     hl                      ; Move HL past terminator

                ; Have we gone across the 8K boundary?
                ld      a,h
                cp      $a0
                jr      c,.next_line
                sub     $20
                ld      h,a

                ; Check to see we haven't broken the 48K boundary
                ld      a,(.smc_page+1)
                cp      6
                jp      nz,.next_page           ; No we're OK!

                ; We've overflow the 48K - error and mark the end of the document.
.overflow:
                call    errorPrint
                dz      "TEXT FILE TOO LARGE!"
                ld      hl,(.last_line)
                dec     hl
                ld      (hl),$ff
                xor     a
                dec     hl
                ld      (hl),a
                dec     hl                      ; Beginning of line number
                ld      (hl),a
                ret

.last_line      dw      0

.done:
                ; Wrap up the file saving
                call    fileDone
                exx
                xor     a
                ldi     (hl),a
                ldi     (hl),a
                dec     a
                ldi     (hl),a

                ; Calculate the length of the document
                ld      a,h
                sub     $80
                ld      h,a                     ; HL = offset into current page
                ld      a,(.smc_page+1)
                dec     a
                or      a
                jr      z,.finish
                ld      b,a
.l3:
                add     hl,$2000
                djnz    .l3

.finish:
                ld      (MainDoc.Length),hl
                ld      a,kInkSuccess
                call    colouredPrint
                dz      C_CLEARLINE,"Text file loaded",C_ENTER

                and     a
                ret

.no_name:
                call    errorPrint
                dz      "No filename given"
                scf
                ret

.line_number:   d24     0

;;----------------------------------------------------------------------------------------------------------------------
;; cmdInsert
;; Start auto-insert mode
;;

cmdInsert:
                ld      a,$ff
                ld      (AutoInsertMode),a

                ; Clear the line and ready for printing the line #
                call    pageVideo
                call    cursorHide
                ld      a,kInkListing
                call    setColour
                ld      a,C_CLEARLINE
                call    printChar

                ; Output the initial line number
                ld      hl,(ix+0)
                ld      a,(ix+2)
                call    printLineNumber

                ; Set up the increment
                inc     ix
                inc     ix
                inc     ix
                inc     ix
                push    ix
                pop     hl
                ld      de,InsertInc
                ld      bc,3
                ldir

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cmdRenumber
;; Renumber from line <line>, and change it to <start>, incrementing the line each time by <inc>.  <line> must be >=
;; line before <start>
;;

cmdRenumber:
                ; (IX+0..2) = start value
                ; (IX+4..6) = increment
                ; (IX+8..10) = first line

                ; Find the first line to start at
                ld      hl,(ix+8)
                ld      c,(ix+10)               ; CHL = first line to start
                push    ix
                call    docFindLine
                pop     ix
                dec     hl
                dec     hl
                dec     hl                      ; HL = address of line #

                ; Renumber the lines
                ld      de,(ix+0)
                ld      c,(ix+2)                ; CDE = Line #

.next_line:
                ; Reached end of file?
                inc     hl
                inc     hl
                ld      a,(hl)
                cp      $ff
                ret     z
                dec     hl
                dec     hl

                ; Write line number
                ldi     (hl),e
                ldi     (hl),d
                ldi     (hl),c

                ; Increment line number
                push    hl
                ld      hl,(ix+4)
                add     hl,de
                ld      a,(ix+6)
                adc     a,c                     ; AHL = next line number
                ex      de,hl
                ld      c,a                     ; CDE = next linu number
                pop     hl

                ; Find the next line
.l1:
                ldi     a,(hl)
                and     a                       ; Reached end of line?
                jr      nz,.l1
                jr      .next_line

;;----------------------------------------------------------------------------------------------------------------------
;; cmdDelete
;; Delete lines
;;

cmdDelete:
                ; If second parmeter is 0, copy 1st parameter into second parameter
                ld      a,(ix+4)
                or      (ix+5)
                or      (ix+6)
                jr      nz,.no_default

                ld      a,(ix+0)
                ld      (ix+4),a
                ld      a,(ix+1)
                ld      (ix+5),a
                ld      a,(ix+2)
                ld      (ix+6),a
.no_default:

                ; Find the fist line
                ld      hl,(ix+0)
                ld      c,(ix+2)                ; CHL = first line to start
                push    ix
                call    docFindLine
                pop     ix
                dec     hl
                dec     hl
                dec     hl
                push    hl                      ; Store start area to delete

                ; Loop through the lines until we find the last line
.next_line:
                ldi     e,(hl)
                ldi     d,(hl)
                ldi     c,(hl)                  ; CDE = current line

                ; Have we reached the end of the document?
                ld      a,c
                cp      $ff
                jr      z,.found_end

                ; Compare it with the end line in (ix+4..6)
                cp      (ix+6)
                jr      c,.not_found
                ld      a,d
                cp      (ix+5)
                jr      c,.not_found
                ld      a,e
                cp      (ix+4)
                jr      z,.not_found
                jr      nc,.found_end

.not_found:
                ; Find the end of this line
                call    strEnd
                inc     hl
                jr      .next_line

.found_end:
                ; HL points past line # of line after deletion area
                ; CF = 0
                dec     hl   
                dec     hl   
                dec     hl

                pop     de              ; DE = start
                sbc     hl,de           ; HL = length
                ex      de,hl
                ld      bc,de
                call    docRemoveSpace
                ret