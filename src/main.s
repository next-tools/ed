;;----------------------------------------------------------------------------------------------------------------------
;; Editor
;; Copyright (C)2020 Matt Davies, all right reserved.
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE          ZXSPECTRUMNEXT
                CSPECTMAP       "ed.map"

;;----------------------------------------------------------------------------------------------------------------------
;; Memory map
;;
;;      +---------------------------------------+ 0000
;;      | Document Page 1                       |
;;      +---------------------------------------+ 2000
;;      | Document Page 2                       |
;;      +---------------------------------------+ 4000
;;      | Document Page 3                       |
;;      +---------------------------------------+ 6000
;;      | Document Page 4                       |
;;      +---------------------------------------+ 8000
;;      | Document Page 5                       |
;;      +---------------------------------------+ a000
;;      | Document Page 6                       |
;;      +---------------------------------------+ c000
;;      | Shared code and stack  | Workspace    |
;;      +---------------------------------------+ e000
;;      | Paged code                            |
;;      +---------------------------------------+
;;

;;----------------------------------------------------------------------------------------------------------------------
;; Includes

                include "z80n.s"
                include "src/shared/defines.s"

                ;;
                ;; Shared code
                ;;

                MMU     6,20
                ORG     $c000

                include "src/shared/memory.s"
                include "src/shared/console.s"
                include "src/shared/document.s"
                include "src/shared/maths.s"
                include "src/shared/page.s"
                include "src/shared/string.s"
                include "src/shared/utils.s"
                include "src/shared/vars.s"

                display "[SHARED] Final address (Max = $deff): ",$," Space: ",$df00-$

                org     $de00
Buffer          ds      256
Stack           ds      256

                SAVEBIN "ed_shared", $c000, $2000

                ;;
                ;; Editor
                ;;

                MMU     7,21
                ORG     $e000

                include "src/editor/keyboard.s"
                include "src/editor/editor.s"
                include "src/editor/commands.s"
                include "src/editor/files.s"
                include "src/editor/tokens.s"

                display "[EDITOR] Final address (Max = $ffff): ",$," Space: ",$10000-$
                SAVEBIN "ed_editor", $e000, $2000

;;----------------------------------------------------------------------------------------------------------------------
;; Start

                ORG     $2000

Start:
                ;;------------------------------------------------------------------------------------------------------
                ;; Set up the application
                ;;------------------------------------------------------------------------------------------------------

                di
                ld      (OldSP),sp
                ld      sp,$3f80

                ; Copy the first argument into the filename buffer
                ld      de,FileName
                call    getArg

                ; Prepare for the start of the DOT command now the stack is set up
                call    dotStart

                ;;------------------------------------------------------------------------------------------------------
                ;; Load in the other sections of code
                ;;------------------------------------------------------------------------------------------------------

                ; Allocate memory to receive code
                call    bootAlloc
                jr      nc,.no_shared_code
                ld      (SharedPage),a
                call    bootAlloc
                jr      nc,.no_editor_code
                ld      (EditorPage),a
                call    bootAlloc
                jr      nc,.no_asm_code
                ld      (AsmPage),a

                ; Load in shared code
                ld      a,(SharedPage)
                page    6,a                     ; Page in area to receive shared code
                dos     M_GETHANDLE
                ld      hl,$c000
                ld      bc,$2000
                dos     F_READ                  ; Read in the code!

                ; Copy the page #s out of DivMMC memory
                ld      hl,SharedPage
                ld      de,SharedCode
                ld      bc,3
                ldir

                ; Load in editor code
                ld      a,(EditorPage)
                page    7,a
                dos     M_GETHANDLE
                ld      hl,$e000
                ld      bc,$2000
                dos     F_READ                  ; Read in the code!

                ; Jump into the editor code
                ld      sp,$e000
                call    editorMain

                ;;------------------------------------------------------------------------------------------------------
                ;; Shut down
                ;;------------------------------------------------------------------------------------------------------

.quit:
                ; Free up memory used by code
                ld      sp,$4000
.no_asm_code:
                ld      a,(AsmPage)
                call    bootFree
.no_editor_code:
                ld      a,(EditorPage)
                call    bootFree
.no_shared_code:
                ld      a,(SharedPage)
                call    bootFree
                jp      dotEnd

;;----------------------------------------------------------------------------------------------------------------------
;; Memory access routines

bootAlloc:
                ; Allocate a page by using the OS function IDE_BANK.
                ; CF = 1 if successful
                ;
                ld      hl,$0001        ; Select allocate function and allocate from normal memory.
                exx                     ; Function parameters are switched to alternative registers.
                ld      de,IDE_BANK     ; Choose the function.
                ld      c,7             ; We want RAM 7 swapped in when we run this function (so that the OS can run).
                rst     8
                db      M_P3DOS         ; Call the function, new page # is in E
                ld      a,e
                ret

bootFree:
                ; Free a page
                ; A = page #
                ld      e,a
                ld      hl,$0003        ; Deallocate function from normal memory
                exx                     ; Function parameters are switched to alternative registers.
                ld      de,IDE_BANK     ; Choose the function.
                ld      c,7
                rst     8
                db      M_P3DOS
                ret

SharedPage      dw      0
EditorPage      dw      0
AsmPage         dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; Bootstrap code

                include "src/shared/dot.s"

;;----------------------------------------------------------------------------------------------------------------------
;; End of code

                display "[BOOT] Final address (Max = $3cff): ",$," Space: ",$3d00-$

;;----------------------------------------------------------------------------------------------------------------------
;; Buffers

                org     $3d00

FileName        ds      256
FileBuffer      ds      256
                ds      256

;;----------------------------------------------------------------------------------------------------------------------
;; Data

                ORG     $0000

                incbin  "data/font.bin"

;;----------------------------------------------------------------------------------------------------------------------
;; Binary file generation

                SAVEBIN "ed_data", $0000, $800
                SAVEBIN "ed_boot", $2000, $2000

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
