;;----------------------------------------------------------------------------------------------------------------------
;; Constants and defines used in .ed
;;----------------------------------------------------------------------------------------------------------------------

kScreenWidth    equ     80
kScreenHeight   equ     32
kScreenTop      equ     $4000

kInkNormal      equ     %000'1'111
kInkError       equ     %000'1'010
kInkTitle       equ     %000'0'100
kInkListing     equ     %000'0'110
kInkSuccess     equ     %000'0'100
