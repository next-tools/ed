;;----------------------------------------------------------------------------------------------------------------------
;; Math routines
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; mul_16_8_24
;; Multiple a 16-bit value by an 8-bit value to get a 24-bit value
;;
;; Input:
;;      HL = 16-bit value
;;      A = 8-bit value
;;
;; Output:
;;      EHL = 24-bit value
;;

;;      HL
;;       A
;;     ---
;;     DEC

mul_16_8_24:
                ld      b,a
                ld      e,a
                ld      d,l
                mul     de      ; DE = L*A
                ld      c,e     ; C = LSB, D = carry
                ld      a,d
                ld      e,b
                ld      d,h
                mul     de
                add     e       ; answer is DAC
                ld      e,d
                ld      h,a
                ld      l,c
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; mul_32_8_40
;; Multiple a 32-bit value by an 8-bit value to get a 24-bit value
;; Taken from Z88DK source code
;;
;; Input:
;;      DEHL = 32-bit value
;;      A = 8-bit value
;;
;; Output:
;;      ADEHL = 40-bit product
;;      CF = 0
;;
;; Affects:
;;      AF,BC,AF'
;; 

mul_32_8_40:
                ; Input and result is now BCHL and ABCHL
                ld      bc,de           ; BC = DE because we use DE for multiplication

                ; First multiply (L*A)
                ld      e,l
                ld      d,a
                mul                     ; E = result, D = carry

                exa
                ld      l,e
                ld      a,d             ; result = xxxxL, A' = carry
                exa

                ; Second multiply (H*A)
                ld      e,h
                ld      d,a
                mul                     ; E = result, D = carry

                exa
                add     a,e             ; Add carry
                ld      h,a             ; result = xxxHL
                ld      a,d             ; next carry
                exa

                ; Third multiply (E*A)
                ld      e,c
                ld      d,a
                mul                     ; E = result, D = carry

                exa
                adc     a,e             ; Add carry
                ld      c,a             ; result = xxCHL
                ld      a,d             ; next carry
                exa

                ; Fourth multiply (D*A)
                ld      e,b
                ld      d,a
                mul     de              ; E = result, D = carry

                exa
                adc     a,e             ; Add carry
                ld      b,a             ; result = xBCHL

                ;ld      a,d
                ;adc     a,0             ; final carry
                adc     a,d
                sub     b

                ; Relocate BC->DE
                ld      de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; unpackD24
;; Convert a 24-bit value into BCD
;;
;; Input:
;;      EHL = 24-bit value
;;
;; Output:
;;      DEHL = BCD of value in 8-digit form
;;
;; Affects:
;;      AF, DE, HL
;;

unpackD24:
                push    bc,ix
                ld      c,e
                push    hl
                pop     ix              ; CIX = 24-bit value
                ld      hl,1
                ld      d,h
                ld      e,h
                ld      b,24            ; 24 bits to process

.l1:
                add     ix,ix
                rl      c               ; Push top bit of CIX into carry
                jr      c,.nextbit
                djnz    .l1             ; Find highest 1-bit

                ; All bits are 0
                res     0,l             ; LSBit not 1
                pop     ix,bc
                ret

.l2:
                ld      a,l
                add     a,a
                daa
                ld      l,a
                ld      a,h
                adc     a,a
                daa
                ld      h,a
                ld      a,e
                adc     a,a
                daa
                ld      e,a
                ld      a,d
                adc     a,a
                daa
                ld      d,a
                add     ix,ix
                rl      c
                jr      nc,.nextbit
                set     0,l
.nextbit        djnz    .l2
                pop     ix,bc
                ret
