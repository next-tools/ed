;;----------------------------------------------------------------------------------------------------------------------
;; DOT command initialisation and shutdown
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Saves state

OldSP           dw      0       ; Original SP
OldIY           dw      0       ; Original IY
OldMMUs         ds      6       ; Original MMU state
OldSpeed        db      0       ; Original clock speed

;;----------------------------------------------------------------------------------------------------------------------
;; dotStart
;;
;; Run after SP has first been saved and set up:
;;
;;              ld (OldSP),sp
;;              ld sp,$....
;;

dotStart:
                ; Wait for the keyboard to be fully released
                in      a,($fe)
                cpl
                and     15
                jr      nz,dotStart

                ; Store IY
                ld      (OldIY),iy

                ; Store the MMU state
                rreg    NR_MMU2
                ld      (OldMMUs+0),a
                rreg    NR_MMU3
                ld      (OldMMUs+1),a
                rreg    NR_MMU4
                ld      (OldMMUs+2),a
                rreg    NR_MMU5
                ld      (OldMMUs+3),a
                rreg    NR_MMU6
                ld      (OldMMUs+4),a
                rreg    NR_MMU7
                ld      (OldMMUs+5),a

                ; Set the state of the MMU to a known state.
                ; This pages in the ULA and sysvars pages.
                page    2,$0a
                page    3,$0b

                ; Set the clock speed to 28MHz (because why the hell not?)
                rreg    NR_CLOCK_SPEED
                and     3
                ld      (OldSpeed),a
                nextreg NR_CLOCK_SPEED,3

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; dotEnd
;;
;; Don't forget to restore interrupts.
;;

dotEnd:
                di

                ; Restore MMU state
                ld      a,(OldMMUs+0)
                page    2,a
                ld      a,(OldMMUs+1)
                page    3,a
                ld      a,(OldMMUs+2)
                page    4,a
                ld      a,(OldMMUs+3)
                page    5,a
                ld      a,(OldMMUs+4)
                page    6,a
                ld      a,(OldMMUs+5)
                page    7,a

                ld      sp,(OldSP)
                ld      iy,(OldIY)
                ld      a,(OldSpeed)
                reg     NR_CLOCK_SPEED,a
                ei
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; getArg
;; Obtain the next argument and initialise a null terminated buffer with that argument.
;;
;; Input:
;;      HL = command line
;;      DE = destination buffer for next argument (256 bytes max)
;; Output:
;;      CF = 0: no argument, 1: argument found
;;      HL = command line following this argument
;;      BC = length of argument (1..255)

getArg:
                ld      bc,0            ; Initialise size to 0
                ld      a,h
                or      l
                ret     z               ; No arguments
.l1:
                ld      a,(hl)          ; Fetch next character from command line
                inc     hl
                and     a               ; $00 found?
                ret     z               ; We're done here!
                cp      $0d             ; Newline?
                ret     z               ; Also, done here!
                cp      ':'             ; Colon?
                ret     z               ; This also finishes.
                cp      ' '
                jr      z,.l1           ; Skip spaces
                cp      '"'             ; Now let's handle quotes
                jr      z,.quoted

.unquoted:
                ld      (de),a          ; Actual character we want to store
                inc     de
                inc     c               ; Increment length (maximum will be 255)
                jr      z,.bad_size     ; Don't allow >255

                ld      a,(hl)
                and     a
                jr      z,.complete
                cp      $0d
                jr      z,.complete
                cp      ':'
                jr      z,.complete
                cp      '"'             ; This quote indicates next arg
                jr      z,.complete

                inc     hl
                cp      ' '
                jr      nz,.unquoted

.complete:
                xor     a
                ld      (de),a          ; terminate the string

                scf                     ; Found argument
                ret

.quoted:
                ld      a,(hl)
                and     a
                jr      z,.complete
                cp      $0d
                jr      z,.complete
                inc     hl
                cp      '"'
                jr      z,.complete     ; Found matching quote

                ld      (de),a
                inc     de
                inc     c
                jr      z,.bad_size     ; Don't allow >255
                jr      .quoted

.bad_size:
                and     a
                ret

