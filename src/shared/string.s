;;----------------------------------------------------------------------------------------------------------------------
;; String processing routines
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; isSpace macro
;;
;; Input:
;;      A = character
;;
;; Output:
;;      CF = 1 if is whitespace
;;

isSpace         macro
                cp      $21
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; isDigit
;; Returns CF = 0 if A is a digit
;;

isDigit:
                cp      '0'
                ret     c
                cp      '9'+1
                ccf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; isIdentChar
;;
;; Input:
;;      A = character
;;
;; Output:
;;      CF = 0 if character is 0-9,a-z,A-Z or _.
;;

isIdentChar:
                call    isDigit
                ret     nc
                cp      '_'
                scf
                ccf
                ret     z
                ; Continues into isAlpha

;;----------------------------------------------------------------------------------------------------------------------
;; isAlpha
;;
;; Input:
;;      A = character
;;
;; Output:
;;       CF = 0 if character is a-z or A-Z
;;

isAlpha:
                call    isUpperAlpha
                ret     nc
                cp      'a'
                ret     c
                cp      'z'+1
                ccf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; isUpperAlpha
;;
;; Input:
;;      A = character
;;
;; Output:
;;      CF = 0 if character is A-Z
;;

isUpperAlpha:
                cp      'A'
                ret     c
                cp      'Z'+1
                ccf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; upperCase
;;
;; Input:
;;      A = character
;;
;; Output:
;;      A = upper case character
;;

upperCase:
                cp      'a'
                ret     c
                cp      'z'+1
                ret     nc
                sub     32
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strSkipWS
;; Advance pointer until it passes all whitespace or hits a null terminator
;;
;; Input:
;;      HL = input
;;
;; Output:
;;      HL = Input past any whitespace
;;

strSkipWS:
                ld      a,(hl)
                and     a
                ret     z
                cp      $21
                ret     nc
                inc     hl
                jr      strSkipWS

;;----------------------------------------------------------------------------------------------------------------------
;; strFindWS
;; Advance pointer until it passes all non-whitespace or hits a null terminator
;;
;; Input:
;;      HL = Input
;;
;; Output:
;;      HL = Points to first whitespace in input or null terminator
;;

strFindWS:
                ld      a,(hl)
                and     a
                ret     z
                cp      $21
                ret     c
                inc     hl
                jr      strFindWS

;;----------------------------------------------------------------------------------------------------------------------
;; strDec
;;
;; Input:
;;      HL = Input buffer
;;
;; Output:
;;      CF=1 on an error (no valid number in buffer)
;;      DEHL = result
;;      BC = points after number
;;

strDec:
                call    strSkipWS

                ; Get first digit
                ld      a,(hl)
                cp      '0'
                ret     c               ; Not a valid digit, fail
                cp      '9'+1
                ccf
                ret     c               ; Not a valid digit, fail
                sub     '0'

                ; A = first digit
                ; HL = character pointer after first digit
                ld      de,0
                ld      bc,hl           ; BC = char*
                ld      h,d
                ld      l,a             ; DEHL = total so far (i.e. the first digit)

.l1:
                ; Get next digit
                inc     bc
                ld      a,(bc)          ; Get next digit
                sub     '0'
                ccf
                ret     nc              ; End of digits
                cp      10
                ret     nc
                push    af
                ld      a,10

                ; DEHL = DEHL * 10
                push    bc
                call    mul_32_8_40
                pop     bc

                ; Add in the digit
                pop     af
                add     a,l
                ld      l,a
                jr      nc,.l1
                inc     h
                jr      nz,.l1
                inc     de
                jr      .l1

;;----------------------------------------------------------------------------------------------------------------------
;; strlen
;; Return the length of the string
;;
;; Input:
;;      HL = buffer
;;
;; Output:
;;      BC = length

strlen:
                push    af,hl
                ld      bc,0
.l1:            ld      a,(hl)
                and     a
                jr      z,.done
                inc     hl
                inc     bc
                jr      .l1
.done:          pop     hl,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strEnd
;; Return the address of the end of string
;;
;; Input:
;;      HL = string
;;
;; Output:
;;      HL = end of string.  (HL) == 0.
;;

strEnd:
                push    af
.l1:            ld      a,(hl)
                and     a
                jr      z,.done
                inc     hl
                jr      .l1
.done:          pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strcpy
;; Copy one string into the buffer of another
;;
;; Input:
;;      HL = source string
;;      DE = destination buffer
;;
;; Output:
;;      HL = end of source string pointing after null terminator
;;      DE = destination buffer after the string that's just been copied
;;      ZF = 1
;;      CF = 0
;;

strcpy:
                ldi     a,(hl)
                ldi     (de),a
                and     a
                ret     z
                jr      strcpy

;;----------------------------------------------------------------------------------------------------------------------
;; strSliceCopy
;; Copy a slice of a string (defined between HL and BC) to a buffer
;;
;; Input:
;;      HL = source string
;;      BC = end of string
;;      DE = destination buffer
;;
;; Output:
;;      HL = end of source string pointing after null terminator
;;      DE = destination buffer after the string that's just been copied
;;

strSliceCopy:
                ld      a,(bc)
                push    af
                xor     a
                ld      (bc),a
                call    strcpy
                pop     af
                ld      (bc),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
