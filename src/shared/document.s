;;----------------------------------------------------------------------------------------------------------------------
;; Document
;; Contains 48K of tokens
;;----------------------------------------------------------------------------------------------------------------------

; $989680 == 10,000,000
kMaxLine0       equ     $98
kMaxLine1       equ     $96
kMaxLine2       equ     $80
kMaxLine        equ     9999999

;;----------------------------------------------------------------------------------------------------------------------
;; Document meta-data structure

                STRUCT Document

Name            ds      256             ; Filename
Length          dw      0               ; Length of document
Pages           ds      7               ; Pages allocated for document

                ENDS

;;----------------------------------------------------------------------------------------------------------------------
;; Gobal data

MainDoc         Document
DummyPage       db      0

;;----------------------------------------------------------------------------------------------------------------------
;; initDoc
;; Initialise an empty document

initDoc:
                ; Set up the dummy page if necessary
                ld      a,(DummyPage)
                and     a
                jr      nz,.dummy_done
                call    allocPage
                ld      (DummyPage),a

.dummy_done:
                ; Allocate pages for the document
                ld      b,6             ; Number of pages
                ld      hl,MainDoc.Pages
.l1:
                call    allocPage
                ldi     (hl),a
                djnz    .l1

                ; Add the dummy page on the end
                ld      a,(DummyPage)
                ld      (hl),a

                ; Test that they all allocated
                ld      hl,MainDoc.Pages
                ld      b,6
.l2:
                ldi     a,(hl)
                and     a
                jr      z,.error
                djnz    .l2

                ; Set length to 0
                ld      hl,MainDoc.Length
                ld      bc,3
                ldi     (hl),c
                ld      (hl),b

                ; Create an end marker
                call    pageDoc
                ld      hl,2
                ld      a,$ff
                ld      (hl),a

                xor     a
                ret
.error:
                ; Deallocate all pages
                call    doneDoc
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; doneDoc
;; Free the memory used by the document
;;
;; TODO: Ensure 7th dummy page is not free if there is more than one document initialised.

doneDoc:
                ld      b,7
                ld      hl,MainDoc.Pages
.l1:
                ldi     a,(hl)
                call    freePage
                djnz    .l1

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; checkLineNumber
;; Check to see if a line number is <= 9,999,999
;;
;; Input:
;;      EHL = line number
;;
;; Output:
;;      CF = 1 if line number is valid
;;

checkLineNumber:
                ld      a,e
                cp      kMaxLine0
                ret     c
                ld      a,h
                cp      kMaxLine1
                ret     c
                ld      a,l
                cp      kMaxLine2
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docFindLine
;; Will find the first line on or after the given line number.  Will page in the document.
;;
;; Input:
;;      CHL = 24-bit line number
;;
;; Output:
;;      HL = Address of start of line.
;;      CDE = Line number of line
;;      ZF = 1 if line found
;;      CF = 1 if line past end of document (ZF = 1 as well)
;;
;; Affects:
;;      IX, B, DE'
;;

docFindLine:
                ld      b,c
                push    hl
                pop     ix              ; BIX = line to find

                call    pageDoc
                ld      hl,0

.next_line:
                ; Get line number
                ldi     de,(hl)
                ldi     c,(hl)

                ; Have we reached the end of the document?
                ld      a,c
                cp      $ff
                scf
                ret     z

                ; Have we reached the required line or gone past it?
                cp      b
                jr      c,.not_found
                ld      a,d
                cp      xh
                jr      c,.not_found
                ld      a,e
                cp      xl
                ret     nc

.not_found:
                ; Search for EOL
                xor     a
.l1:            cp      (hl)
                inc     hl
                jr      nz,.l1
                jr      .next_line


;;----------------------------------------------------------------------------------------------------------------------
;; docInsertLine:
;; Insert a line into the document.  Will page in the document.
;;
;; Input:
;;      DE = input buffer
;;
;; Output:
;;      HL = address of new space.
;;      CF = 1 if error occured (out of memory or line number bad)
;;
;; TODO: Check for OOM state (CF=1 from docInsertSpace)
;; TODO: Do we need to check for a space after a line number?  Seems deleted during tolkenisation
;;

docInsertLine:
                ; First step is to read the line number
                ex      de,hl
                call    strDec                  ; DEHL = line number, BC = buffer
                call    checkLineNumber         ; Is it within valid range?
                jp      nc,.error_number
                swap16  de,bc                   ; CHL = line number, DE = buffer

                ; Skip a space if there is one
                ; TODO: Tokenise input buffer
                ld      a,(de)
                cp      ' '
                jr      nz,.no_space
                inc     de
.no_space:
                ; DE = start of new line
                ; CHL = line number
                ;
                ld      (LineNumber),hl
                ld      a,c
                ld      (LineNumber+2),a
                push    bc
                ex      de,hl
                call    strlen                  ; Get length of line
                ex      de,hl
                ld      (LineLength),bc
                pop     bc
                ld      (LineInput),de

                call    docFindLine             ; HL = start of line on or after requested line
                jr      c,.not_found            ; Reached end of source before line number was reached or passed
                jr      z,.replace_line         ; Found exact line and now we will replace it
                xor     a

.not_found:
                ; CF = 1 if no line found afterwards, 0 = we're inserting line
                ; If CF = 0:
                ;       HL = start of line after new line being inserted
                ;       CDE = line # of the line afterwards
                ;       ZF = 1
                ;
                ;exa
                ld      bc,(LineLength)

                ; COMMENT THESE LINES BACK IN FOR EMPTY LINES TO BE IGNORED
                ;ld      a,b
                ;or      c
                ;ret     z                       ; Nothing to insert if line length is 0
                ;exa                             ; Restore flags

                ; Insert the space requried for the new line
                dec     hl
                dec     hl
                dec     hl                      ; Point to line #
                inc     bc
                inc     bc
                inc     bc
                inc     bc                      ; BC = line length + 3 bytes (for line #) + 1 byte (for terminator)
                call    docInsertSpace
                jr      c,.error_oom

                ld      de,(LineNumber)
                ld      a,(LineNumber+2)
                ldi     (hl),de
                ldi     (hl),a                  ; Write line #
.copy_to_hl:
                ex      de,hl
.copy_to_de:
                ld      hl,(LineInput)
                ld      bc,(LineLength)
                inc     bc
                call    memcpy
                and     a
                ret

.replace_line:
                ; CF = 0
                ; ZF = 1
                ; HL = start of line to be replaced
                ; CDE = line # of the line afterwards
                ;
                call    strlen                  ; BC = length of line being replaced
                ld      de,(LineLength)

                ; COMMENT THESE BACK IN FOR LINE NUMBERS BY THEMSELVES TO DELETE
                ;ld      a,d
                ;or      e
                ;jr      z,.delete_line          ; Delete line if the line numbers match

                ex      de,hl                   ; HL = line length, DE = line to replace
                sbc     hl,bc                   ; HL = number of bytes to remove
                jr      z,.copy_to_de           ; Line length matches so just copy the bytes

                ex      de,hl                   ; HL = line to be replaced, DE = difference of line lengths
                ld      bc,de
                jr      c,.shorter              ; New line is shorter so we need to remove some space
                call    docInsertSpace
                jr      c,.error_oom
                jr      .copy_to_hl

.shorter:
                ; Negate BC
                xor     a
                sub     c
                ld      c,a
                sbc     a,a
                sub     b
                ld      b,a
                call    docRemoveSpace
                jr      .copy_to_hl

.delete_line:
                dec     hl
                dec     hl
                dec     hl
                inc     bc
                inc     bc
                inc     bc
                inc     bc
                jp      docRemoveSpace

.error_number:
                call    errorPrint
                dz      "LINE NUMBER OUT OF RANGE"
                scf
                ret
.error_oom:
                call    errorPrint
                dz      "OUT OF MEMORY"
                scf
                ret

LineLength      dw      0       ; Length of line being inserted
LineNumber      d24     0       ; Line # of new line being inserted
LineInput       dw      0       ; Address of line input

;;----------------------------------------------------------------------------------------------------------------------
;; docInsertSpace
;; Insert space into the document and move everything else up.  Assumes docs are already paged in.
;;
;; Input:
;;      HL = beginning of area to insert space
;;      BC = amount of bytes to insert
;;
;; Output:
;;      CF = 1 if not enough room
;;

docInsertSpace:
                push    bc,de,hl
                and     a
                ld      de,(MainDoc.Length)     ; DE = end of doc
                ex      de,hl
                sbc     hl,de                   ; HL = size to move
                push    hl                      ; Store size to move
                add     hl,de                   ; HL = end of doc
                add     hl,bc                   ; HL = final length
                ld      a,h
                cp      $c0
                ccf
                jr      c,.end                  ; Not enough room

                ; Store final length
                ld      (MainDoc.Length),hl

                ld      hl,de                   ; HL = start of source
                add     hl,bc                   ; HL = start of dest
                ex      de,hl                   ; HL = src, DE = dst
                pop     bc
                call    memcpy_r
                and     a
.end:           pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; docRemoveSpace
;; Remove space from the document and move everything after it down.  Assumes docs are paged in.
;;
;; Input:
;;      HL = beginning of area to remove
;;      BC = amount of bytes to remove
;;

docRemoveSpace:
                push    hl,bc,de
                ld      de,hl           ; DE = destination
                add     hl,bc           ; HL = source
                ld      bc,hl           ; BC = source
                ld      hl,(MainDoc.Length)
                and     a
                sbc     hl,bc           ; HL = length
                swap16  bc,hl
                call    memcpy
                pop     de,bc
                ld      hl,(MainDoc.Length)
                sub     hl,bc
                ld      (MainDoc.Length),hl
                pop     hl
                ret



;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

