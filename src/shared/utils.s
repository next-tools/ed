;;----------------------------------------------------------------------------------------------------------------------
;; Various utilties used by .ed
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; max

max:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       HL = maximum value
        ;       DE = minimum value
        ;       CF = 1 if DE was maximum
        ;
                and     a
                sbc     hl,de
                jr      c,.choose_2nd   ; HL < DE?  Choose DE!
                add     hl,de           ; Restore HL
                ret
.choose_2nd     add     hl,de
                ex      de,hl
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit compare

compare16:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       CF, ZF = results of comparison:
        ;
        ;               CF      ZF      Result
        ;               -----------------------------------
        ;               0       0       HL > DE
        ;               0       1       HL == DE
        ;               1       0       HL < DE
        ;               1       1       Impossible
        ;
                push    hl
                and     a
                sbc     hl,de
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; alignUp8K
;; align a 16-bit value up to the nearest 8K
;;
;; Input:
;;      HL = value
;;
;; Output:
;;      HL = aligned value

alignUp8K:
                push    af
                ld      a,l
                and     a               ; LSB == 0?
                jr      nz,.alignup
                ld      a,h
                and     $1f
                jr      nz,.alignup

                ; Value is already aligned
.end            pop     af
                ret

.alignup:
                ld      l,0
                ld      a,h
                and     $e0
                add     a,$20
                ld      h,a
                jr      .end

;;----------------------------------------------------------------------------------------------------------------------
;; waitNoKey
;; Wait for no key to be pressed
;;

waitNoKey:
                xor     a
                in      a,($fe)
                cpl
                and     $1f                     ; We need lower 5 bits to be 00000 (not pressed)
                jr      nz,waitNoKey
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; waitAnyKey
;; Wait for any key to be pressed
;;

waitAnyKey:
                xor     a
                in      a,($fe)
                cpl
                and     $1f
                jr      z,waitAnyKey
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; breakPressed
;; Return ZF=1 if break is pressed
;;

breakPressed:
                ld      a,$fe
                in      a,($fe)
                and     1
                ret     nz
                ld      a,$7f
                in      a,($fe)
                and     1
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
