;;----------------------------------------------------------------------------------------------------------------------
;; Paging system
;;----------------------------------------------------------------------------------------------------------------------

SharedCode:     db      0               ; Page for shared code at $c000-$dfff
EditorCode:     db      0               ; Page for editor code at $e000-$ffff
AsmCode:        db      0               ; Page for assembler code at $e000-$ffff

DivMMCPage:     db      0

;;----------------------------------------------------------------------------------------------------------------------
;; pageDoc
;; Page the document in  MMU0-5

pageDoc:
                push    af,bc,hl
                xor     a
                out     ($e3),a
                bchilo  6,$50
                ld      hl,MainDoc.Pages
.l1:
                ld      a,c
                ld      (.page),a
                inc     c
                ldi     a,(hl)
                page    0,a
.page           equ     $-1
                djnz    .l1
                pop     hl,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDocSection
;; Page a section of the document to MMU4
;;
;; Input:
;;      B = section (0-5)
;;

pageDocSection:
                push    af,hl
                ld      a,b
                ld      hl,MainDoc.Pages
                add     hl,a
                ld      a,(hl)
                page    4,a
                ld      a,b
                cp      5
                jr      z,.done
                inc     hl
                ld      a,(hl)
                page    5,a
.done:
                pop     hl,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDivMMC
;; Page in the DivMMC

pageDivMMC:
                push    af
                ld      a,(DivMMCPage)
                out     ($e3),a
                page    0,$ff
                page    1,$ff
                page    2,10
                page    3,11
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageOutDivMMC
;; Ensure that DivMMC RAM and esxDOS RAM is paged out
;;

pageOutDivMMC:
                push    af
                xor     a
                out     ($e3),a
                pop     af
                ret

;----------------------------------------------------------------------------------------------------------------------
;; pageVideo
;; Make sure the tilemap is available

pageVideo:
                page    2,10
                page    3,11
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
