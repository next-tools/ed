@echo off
call m.bat
if not errorlevel 1 (
    echo Copying autoexec.bas to SD card...
    \env\hdfmonkey\hdfmonkey.exe put \env\tbblue.mmc etc\autoexec.bas /nextzxos
    \env\cspect\CSpect.exe -basickeys -r -tv -brk -16bit -s28 -w3 -zxnext -nextrom -map=ed.map -mmc=\env\tbblue.mmc
)
