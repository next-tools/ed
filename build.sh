sjasmplus src/main.s -I../env/src --zxnext=cspect --msg=war --fullpath
if [ $? -eq 0 ]; then
    echo Generating dot command... | lolcat
    cat ed_boot ed_shared ed_editor ed_data >> ed
    echo Copying Ed to emulator sd card... | lolcat
    hdfmonkey put ../env/tbblue.mmc ed /dot
    hdfmonkey put ../env/tbblue.mmc docs/ed.gde /docs/guides
    rm ed_boot ed_shared ed_editor ed_data
fi

