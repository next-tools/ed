#!/bin/bash

./build.sh
if [ $? -eq 0 ]; then
    pwd
    mono ../env/cspect/CSpect.exe -basickeys -r -tv -brk -16bit -s28 -w3 -zxnext -nextrom -map=ed.map -mmc=../env/tbblue.mmc
fi

