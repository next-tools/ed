@TITLE Ed
@AUTHOR Matt Davies
@VERSION 0.1
@COPYRIGHT Copyright (C)2020 Matt Davies, all rights reserved.
@DATE 2020-05-08
@-----------------------------------------------------------------------------
@NODE MAIN
@next STARTING
@{c}@{h1}ED MANUAL

Copyright @(2020 Matt Davies, all rights reserved.

Ed is a simple edlin-style editor designed to run on the ZX Spectrum Next.

Please visit the relevant sections of the manual to get start, or read the
pages in order using the next/previous functions of the NextGuide.

 @{" Starting Ed " link STARTING}
 @{" Using the editor " link ED_MAIN}
@-----------------------------------------------------------------------------
@NODE STARTING
@toc MAIN
@prev MAIN
@next ED_MAIN
@{c}@{h1}Starting Ed

Ed is a dot command and as such the file ".ed" should be installed in the
"/dot" folder on your SD card, and all files with the ".gde" extension should
be copied in the "/docs/guides" folder.

Once this is done, Ed can be started by simply typing

    @{b}.ed@{ub}
    
at the command line.  You can add an optional parameter to load a file in
ready for editing by:

    @{b}.ed <filename>@{ub}

Currently there are limits to Ed:

  1) Cannot load files greater than around 40K.  The exact size depends on
     the number of lines and white space in the file.  An error will occur
     if the file cannot be loaded.
  2) Each line cannot be greater than 255 characters and lines of no more than
     72 characters can be changed.  Lines longer than 72 characters, if
     untouched, will retain the full amount of characters.
@-----------------------------------------------------------------------------
@NODE ED_MAIN
@toc MAIN
@prev STARTING
@next ED_BASIC
@{c}@{h1}Using the editor

Please read the relevant section to find help for using the editor.

You may also type 'h' and press ENTER to see a summary of the commands and
keyboard shortcuts allowed by the editor.

 @{" Basic controls " link ED_BASIC}
 @{" Inserting lines " link ED_INSERT_LINE}
 @{" Editor commands " link ED_COMMANDS}
@-----------------------------------------------------------------------------
@node ED_BASIC
@TOC ED_MAIN
@prev ED_MAIN
@next ED_INSERT_LINE
@{c}@{h1}Using the editor

The editor allows you to type anywhere on the screen.  Just use the cursor
keys to move around and then you can type.  At any time you press ENTER, the
current line is activated and the cursor goes to the next line.  This means
you can revisit lines on the screen that you typed before and edit them.

@{h2}Moving the cursor

The cursor keys will move the cursor around as expected, but @{b}Ext+Cursor@{ub} Left
will move to the beginning of the line and @{b}Ext+Cursor Right@{ub} will move to the
end of the line.

@{h2}Clearing the screen

When you reach the bottom of the screen, it will scroll up.  However, you can
clear the screen easily by pressing @{b}Ext+L@{ub}.

@{h2}Overwrite mode

Normally, when you type, characters after your cursor will shift to the right
as you insert new characters before it.  Any characters that are moved of the
edge of the screen are lost forever so be wary of this.  

You can switch to overwrite mode so that the characters on the screen are
just overwritten and not shifted to make room.  This is achieved by pressing
@{b}Ext+O@{ub}.  Press it again to return to insert mode.  You will notice that the
cursor is green during insert mode and red during overwrite mode.

@{h2}Tab

You can insert spaces up to the next tab position by pressing INV VIDEO.  Tab
positions can not currently be changed yet.

@{h2}Deleting characters

Deleting characters on the screen can be done forwards or backwards.  If you
press @{b}DELETE@{ub}, you will delete the character before the cursor.  If you press
@{b}TRUE VIDEO@{ub}, you will delete the character on the cursor and the following
characters will be shifted left to fill its space.

You can also clear the whole line by pressing @{b}Ext+EDIT@{ub}.  Or clear
from your cursor point to the end of the line with @{b}EDIT@{ub}.

@-----------------------------------------------------------------------------
@NODE ED_INSERT_LINE
@TOC ED_MAIN
@prev ED_BASIC
@next ED_COMMANDS
@{c}@{h1}Inserting lines into your source code

@{h2}Adding new lines

The editor uses line numbers to organise the source document.  These line 
numbers range from 0-9,999,999.  To enter a line of text, type its line number
followed by the text you wish to be inserted at that line.  For  example:

    @{b}10 Hello, World!@{ub}

will create a new line with the text "Hello, World!".  Note the space between
the number 10 and the letter H will not be included in your new line.  That
space is optional but when it is included, it is ignored.

@{h2}Replacing lines

If you use the same line number has one that already exists in the code, the
old line will be replaced with the new.  So please take care!
@-----------------------------------------------------------------------------
@node ED_COMMANDS
@toc ED_MAIN
@prev ED_INSERT_LINE
@next ED_DELETE
@{c}@{h1}Editor commands

When pressing @{b}ENTER@{ub} on a line that starts with a line number, that line is
interpreted as a command to insert code into the source document.  However,
if you do not have a line number, the line is interpreted as a command.
Commands take the form of:

    @{b}<command> <parameter 1> <parameter 2> ...@{ub}

That is, a command is a single word optionally followed by parameters.  Each
command expects a certain number of parameters.  Any missing parameters will
be replaced by a default value.

Below is a list of all the commands available.  Each of the nodes below will
show the command syntax, the default values for the parameters and an
explanation of what they do.

 @{" D  Delete lines " link ED_DELETE}
 @{" I  Auto insert lines " link ED_INSERT}
 @{" L  Load file " link ED_LOAD}
 @{" P  Print Listing " link ED_PRINT}
 @{" R  Renumber lines " link ED_RENUMBER}
 @{" S  Save file " link ED_SAVE}

@-----------------------------------------------------------------------------
@node ED_DELETE
@toc ED_COMMANDS
@prev ED_COMMANDS
@next ED_INSERT
@{c}@{h1}Command: D

Delete lines

@{h2}Syntax:
    D <first line> <second line>

@{h2}Default values:
    D 0 0

@{h2}Description:

This command will delete a given range of lines.  However, if only one
parameter is given, only that line will be deleted.
@-----------------------------------------------------------------------------
@node ED_INSERT
@toc ED_COMMANDS
@prev ED_DELETE
@next ED_LOAD
@{c}@{h1}Command: I

Auto-insert lines

@{h2}Syntax:
    I <first line> <increment>

@{h2}Default values:
    I 10 10

@{h2}Description

This command will activate auto-insert mode which will stay activated until
a line is entered without a line number.  This mode auto-inserts the next
line number when you press @{b}ENTER@{ub} so you do not have to.

The first line number will be the same as the first parameter of the command
and after each line is entered, the line number automatically inserted will
increase by the amount given by the second parameter.

To exit this mode, either move your cursor off the line with the line number,
clear the line with @{b}EDIT@{ub}, or clear the screen with @{b}Ext+L@{ub}.
@-----------------------------------------------------------------------------
@node ED_LOAD
@toc ED_COMMANDS
@prev ED_INSERT
@next ED_PRINT
@{c}@{h1}Command: L

Loads the text document

@{h2}Syntax:
    L <filename>

@{h2}Description:

Loads a text document from the file system.

@{b}NOTE: It will replace any document that is already in memory without any
warning.@{ub}
@-----------------------------------------------------------------------------
@node ED_PRINT
@toc ED_COMMANDS
@prev ED_LOAD
@next ED_RENUMBER
@{c}@{h1}Command: P

Prints the source document.

@{h2}Syntax:
    P <first line> <last line>

@{h2}Default values:
    P 0 9999999

@{h2}Description

This will list the lines of the source document with the given range of line 
numbers.  This allows you to move your cursor back over those lines and edit
them again if you so desire.  Typing P on its own will list the entire source
document.
@-----------------------------------------------------------------------------
@node ED_RENUMBER
@toc ED_COMMANDS
@prev ED_PRINT
@next ED_SAVE
@{c}@{h1}Command: R

Renumber lines in the document.

@{h2}Syntax:
    R <start line> <increment> <first line>

@{h2}Default values:
    R 10 10 0

@{h2}Description

This will renumber the lines from <first line> to the end of the source
document to the lines starting with <start line> and incrementing each time
by <increment>

For example:

    10 ; 1st line
    20 ; 2nd line
    30 ; 3rd line

can be renumbered with this command: "R 100 10 20" to give:

     10 ; 1st line
    100 ; 2nd line
    110 ; 3rd line
@-----------------------------------------------------------------------------
@node ED_SAVE
@toc ED_COMMANDS
@prev ED_RENUMBER
@{c}@{h1}Command: S

Saves the text document

@{h2}Syntax:
    S <filename>

@{h2}Description:

Saves a text document to the file system.  If the document has been saved
or loaded before, and hence assigned a filename, then no parameter is
required if the filename is to remain the same.

@{b}NOTE: It will replace any document that is already on the filesystem with the
given name without any warning.@{ub}
@-----------------------------------------------------------------------------